<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once 'Database.php';

class Imoveis{

    // database connection and table name
    private $conn;
    private $table_name = "Imoveis";

    // object properties
    public $Id;
	public $Categoria;
	public $QuantidadeBanheiros;
	public $QuantidadeQuartos;
	public $DataAlteracao;
	public $VendaAluguel;
	public $Detalhes;
	public $ClienteProprietario;
	public $DataCriacao;
	public $AnoConstrucao;
	public $Descricao;
	public $Ativo;
	public $Tipo;
	public $CaminhoImagens;
	public $TamanhoImovel;
	public $TamanhoAreaTotal;
	public $Recursos;
	public $Endereco;
	public $Valor;

            // Id
            // ,Categoria
            // ,QuantidadeBanheiros
            // ,QuantidadeQuartos
            // ,DataAlteracao
            // ,VendaAluguel
            // ,Detalhes
            // ,ClienteProprietario
            // ,DataCriacao
            // ,AnoConstrucao
            // ,Descricao
            // ,Ativo
            // ,Tipo
            // ,CaminhoImagens
            // ,TamanhoImovel
            // ,TamanhoAreaTotal
            // ,Recursos
            // ,Endereco
            // ,Valor

    function Insert($values){
		$query = "INSERT INTO 
        Imoveis (
            Categoria
            ,QuantidadeBanheiros
            ,QuantidadeQuartos
            ,DataAlteracao
            ,VendaAluguel
            ,Detalhes
            ,ClienteProprietario
            ,DataCriacao
            ,AnoConstrucao
            ,Descricao
            ,Ativo
            ,Tipo
            ,CaminhoImagens
            ,TamanhoImovel
            ,TamanhoAreaTotal
            ,Recursos
            ,Endereco
            ,Valor) Values (
                '{$values['Categoria']}',
                '{$values['QuantidadeBanheiros']}',
                '{$values['QuantidadeQuartos']}',
                '{$values['DataAlteracao']}',
                '{$values['VendaAluguel']}',
                '{$values['Detalhes']}',
                '{$values['ClienteProprietario']}',
                '{$values['DataCriacao']}',
                '{$values['AnoConstrucao']}',
                '{$values['Descricao']}',
                '{$values['Ativo']}',
                '{$values['Tipo']}',
                '{$values['CaminhoImagens']}',
                '{$values['TamanhoImovel']}',
                '{$values['TamanhoAreaTotal']}',
                '{$values['Recursos']}',
                '{$values['Endereco']}',
                '{$values['Valor']}')";

	    echo $query;
		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// execute query
		$stmt->execute();

		return $stmt;
	}

	function Select(){

		// select all query
		$query = "SELECT
				  Id
                ,im.Categoria
                ,im.QuantidadeBanheiros
                ,im.QuantidadeQuartos
                ,im.DataAlteracao
                ,im.VendaAluguel
                ,im.Detalhes
                ,im.ClienteProprietario
                ,im.DataCriacao
                ,im.AnoConstrucao
                ,im.Descricao
                ,im.Ativo
                ,im.Tipo
                ,im.CaminhoImagens
                ,im.TamanhoImovel
                ,im.TamanhoAreaTotal
                ,im.Recursos
                ,im.Endereco
                ,im.Valor
				    FROM Imoveis im
				 ";

		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// execute query
		$stmt->execute();

		return $stmt;
	}

    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }
}


if($_SERVER['REQUEST_METHOD'] == 'POST'){
	$database = new Database();
    $db = $database->getConnection();

    $Imovel = new Imoveis($db);

	 $Imovel->Insert($_POST);

    // set response code - 200 OK
    header('HTTP/1.0 200');
}

if($_SERVER['REQUEST_METHOD'] == 'GET'){
    $database = new Database();
    $db = $database->getConnection();

    $Imovel = new Imoveis($db);

    $stmt = $Imovel->Select();
    $num = $stmt->rowCount();

    // products array
    $imoveis_arr=array();
    $imoveis_arr["records"]=array();

    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){

        extract($row);

        $imoveis_item=array(
             "Id" => $Id,
            "Categoria" => $Categoria,
            "QuantidadeBanheiros" => $QuantidadeBanheiros,
            "QuantidadeQuartos" => $QuantidadeQuartos,
            "DataAlteracao" => $DataAlteracao,
            "VendaAluguel" => $VendaAluguel,
            "Detalhes" => $Detalhes,
            "ClienteProprietario" => $ClienteProprietario,
            "DataCriacao" => $DataCriacao,
            "AnoConstrucao" => $AnoConstrucao,
            "Descricao" => $Descricao,
            "Ativo" => $Ativo,
            "Tipo" => $Tipo,
            "CaminhoImagens" => $CaminhoImagens,
            "TamanhoImovel" => $TamanhoImovel,
            "TamanhoAreaTotal" => $TamanhoAreaTotal,
            "Recursos" => $Recursos,
            "Endereco" => $Endereco,
            "Valor" => $Valor
        );

        array_push($imoveis_arr["records"], $imoveis_item);
    }

    // set response code - 200 OK
    header('HTTP/1.0 200');

    // show products data in json format
    echo json_encode($imoveis_arr);
}

