<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once 'Database.php';

class Clientes{
 
    // database connection and table name
    private $conn;
    private $table_name = "Clientes";
 
    // object properties
	public $Id;
	public $Nome;
	public $DataNascimento;
	public $Profissao;
	public $EstadoCivil;
	public $Nacionalidade;
	public $CpfCnpj;
	public $Rg;
	public $PessoaFisica;
	public $Relacao;
	public $Endereco;
	public $Ativo;
	public $UsuarioCadastro;
	public $UsuarioAlteracao;
	public $DataCriacao;
	public $DataAlteracao;
	public $Email;
	public $Telefone;
	public $Celular;

    function Insert($values){
		
		$query = "INSERT INTO Clientes (Nome,
		                                DataNascimento,
		                                Profissao,
		                                EstadoCivil,
		                                Nacionalidade,
		                                CpfCnpj,
		                                Rg,
		                                PessoaFisica,
		                                Relacao,
		                                Endereco,
		                                Ativo) Values (
										'{$values['Nome']}',
										'{$values['DataNascimento']}',
										{$values['Profissao']},
										{$values['EstadoCivil']},
										'{$values['Nacionalidade']}',
										'{$values['CpfCnpj']}',
										'{$values['Rg']}',
										 1,
										 1,
										 1,
										 true)
		";
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);
	 
		// execute query
		$stmt->execute();
	 
		return "[{Id:{$this->conn->lastInsertId()}}]";
	}

	function Select(){
	 
		// select all query
		$query = "SELECT  
				cl.Id,
				cl.Nome,
				cl.DataNascimento,
				cl.Profissao,
				cl.EstadoCivil,
				cl.Nacionalidade,
				cl.CpfCnpj,
				cl.Rg,
				cl.PessoaFisica,
				cl.Relacao,
				cl.Endereco,
				cl.Ativo,
				cl.UsuarioCadastro,
				cl.UsuarioAlteracao,
				cl.DataCriacao,
				cl.DataAlteracao,
				ct.Email,
				ct.Telefone,
				ct.Celular
						 FROM Clientes cl 
						 LEFT JOIN Contato ct ON cl.Id = ct.Cliente
			    WHERE cl.Ativo = true
				 ";
	 
		// prepare query statement
		$stmt = $this->conn->prepare($query);
	 
		// execute query
		$stmt->execute();
	 
		return $stmt;
	}
	
	function Delet($idCliente){
	 
		// select all query
		$query = "UPDATE Clientes SET Ativo = 0 WHERE Id = {$idCliente}";
	 
		// prepare query statement
		$stmt = $this->conn->prepare($query);
	 
		// execute query
		$stmt->execute();
	 
		return $stmt;
	}
	
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }
}

if($_SERVER['REQUEST_METHOD'] == 'DELETE'){
	$database = new Database();
    $db = $database->getConnection();
    
    $cliente = new Clientes($db);
	
	$cliente->Delet($_GET[Id]);

    // set response code - 200 OK
    header('HTTP/1.0 200');
	
}


if($_SERVER['REQUEST_METHOD'] == 'POST'){
	$database = new Database();
    $db = $database->getConnection();
    
    $cliente = new Clientes($db);
	
	header('HTTP/1.0 200');
	
	echo $cliente->Insert($_POST);
}

if($_SERVER['REQUEST_METHOD'] == 'GET'){
    $database = new Database();
    $db = $database->getConnection();
    
    $cliente = new Clientes($db);
    
    $stmt = $cliente->Select();
    $num = $stmt->rowCount();

    // products array
    $clientes_arr=array();
    $clientes_arr["records"]=array();
 
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){

        extract($row);
 
        $clientes_item=array(
            "Id"  => $Id,
		    "Nome"=> $Nome,
		    "DataNascimento"=> $DataNascimento,
		    "Profissao"=> $Profissao,
		    "EstadoCivil"=> $EstadoCivil,
		    "Nacionalidade"=> $Nacionalidade,
		    "CpfCnpj"=> $CpfCnpj,
		    "Rg"=> $Rg,
            "PessoaFisica" => $PessoaFisica?"true":"false",
            "Relacao" => $Relacao,
            "Endereco" => $Endereco,
            "Ativo" => $Ativo,
            "UsuarioCadastro" => $UsuarioCadastro,
            "UsuarioAlteraca" => $UsuarioAlteracao,
            "DataCriacao" => $DataCriacao,
            "DataAlteracao" => $DataAlteracao,
			"Email" => $Email,
			"Telefone" => $Telefone,
			"Celular" => $Celular
        );
 
        array_push($clientes_arr["records"], $clientes_item);
    }
 
    // set response code - 200 OK
    header('HTTP/1.0 200');
 
    // show products data in json format
    echo json_encode($clientes_arr);
}

